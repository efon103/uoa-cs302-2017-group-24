*README.MD*

```
         MMMMM
         MMMMM
         MMMMM
         MMMMM
         MMMMM
         MMMMM
         MMMMM
.dMMd.   MMMMM
NMMMMN   MMMMM
'dMMd'   MMMMM 
```
# dotL
## Defense of the Lords
### A Game by Eugene and William

[.jar executable](http://tofoo.co/files/dotL.jar)

Global Input(s):
```
ENTER KEY: Restarts the game when the timer reaches 0 or a player has won.
P KEY: Pauses/Unpauses the game while running.
PG DOWN KEY: Sets timer to 0 to end the game quickly.
```
Player 1 Input(s):
```
1 KEY: Moves the paddle to the left.
3 KEY: Moves the paddle to the right.
```
Player 2 Input(s):
```
C KEY: Moves the paddle to the left.
B KEY: Moves the paddle to the right.
```
Player 3 Input(s):
```
7 KEY: Moves the paddle to the left.
9 KEY: Moves the paddle to the right.
```
Player 4 Input(s):
```
LEFT KEY: Moves the paddle to the left.
RIGHT KEY: Moves the paddle to the right.
```

Steps to initialise libraries/environment:

1. Run Eclipse Mars Java
2. Click File >> Import >> double-click General >> Existing projects into Workspace
3. Click Browse... and navigate to .../uoa-cs302-2017-group-24/dotL/ and click finish
4. Right click the project folder in package explorer (uoa-cs302-2017-group-24) and click properties
5. Click 'Java Build Path' and click on the Libraries tab
6. Click on JRE System Library [jre1.7.0_xx] if it is not running jre1.8.0_91 otherwise skip to step (10)
7. Click 'Edit' and click Installed JREs then click 'Add' and select 'Standard VM'
8. Click directory and navigate to /usr/lib/jvm/jdk1.8.0_91 then click OK
9. Click Finish then tick the JDK you just added and click finish again
10. Click on the drop down arrow for JRE System Library
11. Double-click 'Access rules' and click 'Add'
12. Change Resolution from 'Forbidden' to 'Accessible'
13. Type javafx/** then click OK until you are back in the Libraries tab
14. Right click the 'resources/' folder in the package explorer and select Build Path >> Use as Source Folder

To compile and run the game:

1. Click the drop down arrow for the project
2. Click the drop down arrow for src
3. To run the game, right click the 'main' package and run as Java Application

If using a Windows machine:

1. Place run.bat into the root of the folder with the compiled game (i.e. /bin)
2. Execute run.bat