package entity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import main.GameController;
import main.GameWindow;


/**
 * The ball class holds information about a specific ball in the dotL game, including its
 * x-coordinate, y-coordinate, x-velocity, y-velocity, size and colour.
 * @author William Chao and Eugene Fong
 *
 */
public class Ball {

	/**
	 * The ball's current top-left x position
	 */
	private double x;

	/**
	 * The ball's current top-left y position
	 */
	private double y;

	/**
	 * The ball's current velocity in the x direction
	 */
	private double dx;

	/**
	 * The ball's current velocity in the y direction
	 */
	private double dy;

	/**
	 * The size of the ball in pixels
	 */
	private int size;

	/**
	 * The colour of the ball
	 */
	private Color c;

	/**
	 * Default constructor for the ball class initialises all private attributes to 0 and sets its colour to transparent.
	 */
	public Ball() {
		x = 0;
		y = 0;
		dx = 0;
		dy = 0;
		size = 0;
		c = Color.TRANSPARENT;
	}

	/**
	 * Ball constructor with inputs
	 * @param x
	 * @param y
	 * @param dx
	 * @param dy
	 * @param c
	 */
	public Ball(int x, int y, int dx, int dy, Color c) {
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.c = c;
		size = 24;
	}

	/**
	 * Sets the colour of the ball to the given colour
	 * @param c
	 */
	public void setColour(Color c) {
		this.c = c;
	}

	/**
	 * Sets the size of the ball to the given size
	 * @param size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 *  Set the x position of the ball to the given x position
	 * @param x
	 */
	public void setXPos(int x) {
		if(!((x < 0) || (x >= GameWindow.WIDTH-size))) {
			this.x = x;
		}
	}

	/**
	 * Set the y position of the ball to the given y position
	 * @param y
	 */
	public void setYPos(int y) {
		if(!((y < 0) || (y >= GameWindow.HEIGHT-size))) {
			this.y = y;
		}
	}

	/**
	 * @return the x position of the ball.
	 */
	public double getXPos() {
		return x;
	}

	/**
	 * @return the y position of the ball.
	 */
	public double getYPos() {
		return y;
	}
	
	/**
	 * @return the size of the ball.
	 */
	public double getSize() {
		return size;
	}

	/**
	 * Set the x velocity of the ball to the given value.
	 * @param dX
	 */
	public void setXVelocity(int dX) {
		dx = dX;
	}

	/**
	 *  Set the y velocity of the ball to the given value.
	 * @param dY
	 */
	public void setYVelocity(int dY) {
		dy = dY;
	}

	/**
	 * @return the x velocity of the ball.
	 */
	public double getXVelocity() {
		return dx;
	}

	/**
	 * @return the y velocity of the ball.
	 */
	public double getYVelocity() {
		return dy;
	}
	
	/**
	 * @return the colour of the ball.
	 */
	public Color getColor(){
		return this.c;
	}


	/**
	 * Checks for boundaries and changes direction if it will exceed the boundary. Plays appropriate sound when ball hits boundary.
	 * Increments the ball's x and y position x its x and y velocity respectively.
	 */
	public void update() {
		
		if (x + dx <= 0) {
			GameController.playSound("/b.wav");
			x = 0;
			dx = -dx;
		} else if (x + dx >= GameWindow.WIDTH-size) {
			GameController.playSound("/b.wav");
			x = GameWindow.WIDTH-size;
			dx = -dx;
		}

		if (y + dy <= 0) {
			GameController.playSound("/b.wav");
			y = 0;
			dy = -dy;
		} else if (y + dy >= GameWindow.HEIGHT-size) {
			GameController.playSound("/b.wav");
			y = GameWindow.HEIGHT-size;
			dy = -dy;
		}
		
		x += dx;
		y += dy;
		
	}

	/*** Boolean registerHit(Rectangle hitBox) detects if the ball has collided with another object with the given hitbox. */

	/**
	 * Checks if the ball will hit the other rectangle at its current x and y velocity.
	 * It changes x and y velocity's direction if it does intersect with the other rectangle.
	 * Rather than incrementing its position x its velocity, it sets the x and/or y position to
	 * the edge of the other object.
	 * @param hitBox - a rectangle 
	 * @return true if the ball intersects with another object of type rectangle
	 */
	
	private double xtemp;
	private double ytemp;

	public Boolean registerHit(Rectangle hitBox) {

		Boolean isHit = false;
		
		xtemp = x;
		ytemp = y;
		
		/*
		 * Checks the ball's next x position only if it is between the other rectangle's y position.
		 * If the ball is moving in the positive x direction (right) we first check if it is to the left
		 * of the object then we check the next top right and bottom right corners of the ball to see if it will
		 * intersect with the other object's x-position. Likewise, if the ball is moving in the negative x
		 * direction (left) we first check if it is to the right of the object then we check the next top left
		 * and bottom left corners of the ball to see if it will intersect with the other object's x-position
		 * and its width.
		 * 
		 * If the ball will intersect with the object, we set the temporary x value to the left/right edge of the
		 * object depending on the current ball's direction, invert the x direction and set isHit to true.
		 */
		if(dx > 0) {
			if (((y + size) >= (int) hitBox.getY()) && (y <= (int) (hitBox.getY()+hitBox.getHeight()) )) {
				if (x <= (int) hitBox.getX()) {
					if ((x + dx + size) >= (int) hitBox.getX()) {
						isHit = true;
						dx = -dx;
						xtemp = (int) (hitBox.getX() - 1 - size);
					}
				}
			}
		} else if (dx < 0) {
			if (((y+size) >= (int) hitBox.getY()) && (y <= (int) (hitBox.getY()+hitBox.getHeight()) )) {
				if (x >= (int) (hitBox.getX() + hitBox.getWidth())) {
					if ((x + dx) <= (int) (hitBox.getX() + hitBox.getWidth())) {
						isHit = true;
						dx = -dx;
						xtemp = (int) (hitBox.getX() + hitBox.getWidth() + 1);
					}
				}
			}
		}
		
		/*
		 * Similarly, we check the ball's next y position only if it is between the other rectangle's x position.
		 * If the ball is moving in the positive y direction (down) we first check if it is above the object then
		 * we check the next bottom left and bottom right corners of the ball to see if it will intersect with the other
		 * object's y-position. Likewise, if the ball is moving in the negative y direction (up) we first check if
		 * it is below the object then we check the next top left and top right corners of the ball to see if it will
		 * intersect with the other object's y-position and its height.
		 * 
		 * If the ball will intersect with the object, we set the temporary y value to the top/bottom edge of the object
		 * depending on the current ball's direction, invert the y direction and set isHit to true.
		 */
		if(dy > 0) {
			if (((x+size) >= (int) hitBox.getX()) && (x <= (int) (hitBox.getX()+hitBox.getWidth()))) {
				if ((y+size) <= hitBox.getY()) {
					if ((y + dy + size) >= (int) hitBox.getY()) {
						isHit = true;
						dy = -dy;
						ytemp = (int) (hitBox.getY() - 1 - size);
					}
				}
			}
		} else if (dy < 0) {
			if (((x+size) >= (int) hitBox.getX()) && (x <= (int) (hitBox.getX()+hitBox.getWidth()))) {
				if (y >= (int) (hitBox.getY() + hitBox.getHeight())) {
					if ((y + dy) <= (int) (hitBox.getY() + hitBox.getHeight())) {
						isHit = true;
						dy = -dy;
						ytemp = (int) (hitBox.getY() + hitBox.getHeight() + 1);
					}
				}
			}
		}
		
		/*
		 * Updates the x and y position if the ball does intersect the other object otherwise they will
		 * remain the same.
		 */
		x = xtemp;
		y = ytemp;
		
		return isHit;
	}

	/**
	 * Draws the ball to the canvas given the GraphicsContext that is related to the canvas.
	 * Sets the GraphicsContext's fill colour to the ball colour and fills an oval at the current
	 * x and y position of the ball with the same width/height given x size so that it is a circle.
	 */
	public void draw(GraphicsContext gc) {
		gc.setFill(c);
		gc.fillOval(x, y, size, size);
	}
}
