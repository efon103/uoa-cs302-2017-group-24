package entity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.GameWindow;

/**
 * The paddle class holds information about a specific paddle in the dotL game, including its
 * current x-position, y-position, width, height, x velocity, y velocity, angle, player number,
 * inputs (left, right, up, down) and its colour.
 * @author William Chao and Eugene Fong
 *
 */
public class Paddle {

	/**
	 * The paddle's width
	 */
	private int width;

	/**
	 * The paddle's height
	 */
	private int height;

	/**
	 * The paddle's x-velocity
	 */
	private int dx;

	/**
	 * The paddle's position starting from the horizontal component and ending at the vertical component
	 */
	private int pos;

	/**
	 * The player that the paddle is assigned to
	 */
	private int player;

	/**
	 * The colour of the paddle relative to the player
	 */
	private Color c;

	/**
	 * The paddle's x position
	 */
	private double x = 0;

	/**
	 * The paddle's y positon
	 */
	private double y = 0;

	/**
	 * The vertical paddle
	 */
	private Rectangle vert = new Rectangle();

	/**
	 * The horizontal paddle
	 */
	private Rectangle hori = new Rectangle();

	/**
	 * The standard reference size set by the game window
	 */
	private int size;

	/**
	 * Default constructor of the paddle class initialises all private attributes to 0
	 * and sets its colour to transparent.
	 */
	public Paddle() {
		player = 0;
		dx = 0;
		width = 0;
		height = 0;
		c = Color.TRANSPARENT;
	}

	/**
	 * Alternative constructor taking inputs. Sets x and y velocity to 8. Also sets the paddle's
	 * colour according to the player given.
	 * @param x - x position of the paddle
	 * @param y - y position of the paddle
	 * @param player - the player related to this paddle
	 */
	public Paddle(int x, int y, int player){
		size = GameWindow.SIZE;

		this.player = player;
		this.dx = 8;
		this.width = size*3;
		this.height = size/2;

		reset(); //set paddle to their start position

	}

	//reset moves the paddle back to it's starting position after a game is restarted.
	public void reset(){

		//set width of the paddles to be the same.
		hori.setHeight(height);
		vert.setWidth(height);

		switch (player) {

		//initial position of player 1 (Top-Left)
		case 1:

			this.c = Color.web("c83737");		//player colour is set

			//set "middle position"/starting position. This is where the paddle will be closest to the ball at spawn
			pos = 272;
			x = 272;
			y = 56;

			//set initial size of the vertical and horizontal paddle as both are shown
			hori.setX(x);
			hori.setY(GameWindow.HEIGHT/3);
			hori.setWidth(GameWindow.WIDTH/size*10-x);

			vert.setX(size*10);
			vert.setY(GameWindow.HEIGHT/3+size/4-y);
			vert.setHeight(y+size/4);
			break;

			//initial position of player 2 (Bottom-Left)
		case 2:

			this.c = Color.web("00aad4");		//player colour is set

			//set "middle position"/starting position. This is where the paddle will be closest to the ball at spawn
			pos = 272;
			x = 272;
			y = 56;

			//set initial size of the vertical and horizontal paddle as both are shown
			hori.setX(x);
			hori.setY(2*GameWindow.HEIGHT/3-size/2);
			hori.setWidth(GameWindow.WIDTH/size*10-x);

			vert.setX(size*10);
			vert.setY(2*GameWindow.HEIGHT/3+size+size/4-y);
			vert.setHeight(y+size/4);
			break;

			//initial position of player 3 (Top-Right)
		case 3:

			this.c = Color.web("ff6600");		//player colour is set

			//set "middle position"/starting position. This is where the paddle will be closest to the ball at spawn
			pos = 272;
			x = 272;
			y = 56;

			//set initial size of the vertical and horizontal paddle as both are shown
			hori.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
			hori.setY(GameWindow.HEIGHT/3);
			hori.setWidth((GameWindow.WIDTH/32*10+size/2)-x);

			vert.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
			vert.setY(GameWindow.HEIGHT/3+size/4-y);
			vert.setHeight(y+size/4);
			break;

			//initial position of player 3 (Bottom-Right)
		case 4:

			this.c = Color.web("008000");		//player colour is set

			//set "middle position"/starting position. This is where the paddle will be closest to the ball at spawn
			pos = 272;
			x = 272;
			y = 56;

			//set initial size of the vertical and horizontal paddle as both are shown
			hori.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
			hori.setY(2*GameWindow.HEIGHT/3-height);
			hori.setWidth((GameWindow.WIDTH/32*10+size/2)-x);

			vert.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
			vert.setY(2*GameWindow.HEIGHT/3-height);
			vert.setHeight(y+size/4);
			break;
		}
	}

	/**
	 * @return the position of the paddle, for AI to use and determine location of the paddle
	 */
	public int getPos(){
		return pos;
	}

	/**
	 * @return the maximum velocity of the paddle.
	 */
	public int getDx(){
		return dx;
	}


	/**
	 * @return the width of the paddle
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * 
	 * @return the height of the paddle
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Draws the paddle to the canvas that the GraphicsContext
	 * is related to according to the properties of the horizontal and vertical paddle  (i.e. current colour, x position,
	 * y position, width and height.)
	 * @param gc
	 */
	public void draw(GraphicsContext gc) {
		gc.setFill(c);
		gc.fillRect(hori.getX(), hori.getY(), hori.getWidth(), hori.getHeight());
		gc.fillRect(vert.getX(), vert.getY(), vert.getWidth(), vert.getHeight());

	}

	/**
	 * Sets the bool left/right/up/down to true if the current key being
	 * pressed relates to left/right/up/down. Reverses the x and/or y velocity
	 * if it is currently set to the opposite direction of the key being pressed.
	 * Calls  update() to move the paddle.
	 * @param Key - current key being pressed
	 */
	public void keyPressed(String Key) {

		//player 1 and player 2 have standard left/right controls
		if ((player == 1) || (player == 2)){
			if (Key == "LEFT") {
				if(dx > 0) {
					dx = -dx;
				}
				update();
			}

			if (Key == "RIGHT") {
				if(dx < 0) {
					dx = -dx;
				}
				update();
			}

			if (Key == "HALT") {
				update();
			}
		}

		//player 3 and player 4 have mirrored controls (i.e. right/left) as the rightmost point is the origin
		else if ((player == 3) || (player == 4)){
			if (Key == "LEFT") {
				if(dx < 0) {
					dx = -dx;
				}
				update();
			}

			if (Key == "RIGHT") {
				if(dx > 0) {
					dx = -dx;
				}
				update();
			}

			if (Key == "HALT") {
				update();
			}
		}
	}

	/**
	 * @return a horizontal rectangle at the paddle's x-y position with its width and height
	 */
	public Rectangle getHitBoxHori() {
		return new Rectangle(hori.getX(), hori.getY(), hori.getWidth(), hori.getHeight()); 
	}

	/**
	 * @return a vertical rectangle at the paddle's x-y position with its width and height
	 */
	public Rectangle getHitBoxVert() {
		return new Rectangle(vert.getX(), vert.getY(), vert.getWidth(), vert.getHeight()); 
	}

	/**
	 * Checks the position of the paddle and moves vertical and horizontal paddles accordingly.
	 * fixes x position to maximum wall columns and y position to maximum wall rows. Ensures
	 * paddles does not move further than the boundaries of the player's base.
	 */

	public void update() {

		//add to position
		pos += dx;

		//if it is at the origin (horizontal limit), do not go any further
		if (pos < 0){
			pos = 0;
			x = 0;
		}

		//between the origin and corner, move horizontal paddle only.
		else if ((pos >= 0) && (pos < 7*size)){
			x += dx;
			y = 0;
		}

		//at the corner, move both horizontal paddle and vertical paddle at the same rate
		else if ((pos >= 7*size) && (pos < 10*size+8)){
			x += dx;
			y += dx;
		}

		//at the end of the corner, move only the vertical paddle and fix horizontal paddle to maximum position
		else if ((pos >= 10*size+8) && (pos < 480)){
			x = 328;
			y += dx;
		}

		//if paddles reaches to the end point (vertical limit), do not go any further
		else if ((pos >= 480)){
			pos = 480;
			y = 256;
		}

		//set position and dimensions for the respective player paddles
		switch (player) {

		//player 1
		case 1:
			//only show x paddle when position is on o
			if ((pos >= 0) && (pos < 7*size)){			
				hori.setX(x);
				hori.setWidth(width);
				vert.setY(0);
				vert.setHeight(0);
			}
			//in the corner, move x towards horizontal limit and move y
			else if ((pos >= 7*size) && (pos < 10*size+8)){
				hori.setX(x);
				hori.setWidth((320)-x);
				vert.setY(256+8-y);
				vert.setHeight(y+8);
			}
			//move vertical only after the corner
			else if ((pos >= 10*size+8) && (pos < 480)){
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(GameWindow.HEIGHT/3+8-y);
				vert.setHeight(width);
			}
			//stop vertical when it reaches the boundary
			else{
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(0);
				vert.setHeight(width);
			}
			break;
			
		//player 2
		case 2:
			//only show x paddle when position is on o
			if ((pos >= 0) && (pos < 7*size)){			
				hori.setX(x);
				hori.setWidth(width);
				vert.setY(0);
				vert.setHeight(0);
			}
			//in the corner, move x towards horizontal limit and move y
			else if ((pos >= 7*size) && (pos < 10*size+8)){
				hori.setX(x);
				hori.setWidth((320)-x);
				vert.setY(2*GameWindow.HEIGHT/3-size/2);
				vert.setHeight(y+8);
			}
			//move vertical only after the corner
			else if ((pos >= 10*size+8) && (pos < 480)){
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(2*GameWindow.HEIGHT/3-size/2+y-80);
				vert.setHeight(width);
			}
			//stop vertical when it reaches the boundary
			else {
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(GameWindow.HEIGHT-width);
				vert.setHeight(width);
			}
			break;
			
		//player 3
		case 3:
			//only show x paddle when position is on o
			if ((x >= 0) && (x < 7*size)){			
				hori.setX(GameWindow.WIDTH-width-x);
				hori.setWidth(width);
				vert.setY(0);
				vert.setHeight(0);
			}
			//in the corner, move x towards horizontal limit and move y
			else if ((pos >= 7*size) && (pos < 10*size+8)){
				hori.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
				hori.setWidth(GameWindow.WIDTH/32*10+size/2-x);
				vert.setY(256+8-y);
				vert.setHeight(y+8);
			}
			//move vertical only after the corner
			else if ((pos >= 10*size+8) && (pos < 480)){
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(256+8-y);
				vert.setHeight(width);
			}
			//stop vertical when it reaches the boundary
			else{
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(0);
				vert.setHeight(width);
			}
			break;
			
		//player 4
		case 4:
			//only show x paddle when position is on o
			if ((x >= 0) && (x < 7*size)){			
				hori.setX(GameWindow.WIDTH-width-x);
				hori.setWidth(width);
				vert.setY(0);
				vert.setHeight(0);
			}
			//in the corner, move x towards horizontal limit and move y
			else if ((pos >= 7*size) && (pos < 10*size+8)){
				hori.setX(GameWindow.WIDTH-(GameWindow.WIDTH/32*10+size/2));
				hori.setWidth(GameWindow.WIDTH/32*10+size/2-x);
				vert.setY(2*GameWindow.HEIGHT/3-size/2);
				vert.setHeight(y+8);
			}
			//move vertical only after the corner
			else if ((pos >= 10*size+8) && (pos < 480)){
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(2*GameWindow.HEIGHT/3-size/2+y-80);
				vert.setHeight(width);
			}
			//stop vertical when it reaches the boundary
			else{
				hori.setX(0);
				hori.setWidth(0);
				vert.setY(GameWindow.HEIGHT-width);
				vert.setHeight(width);
			}
			break;
		}
	}

}
