package entity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * The wall class holds information about a specific wall in the dotL game, including their x position,
 * y position, whether it is destroyed or not, the player it is related to, the colour of the wall, the
 * health of the wall and the size of the wall.
 * @author William Chao and Eugene Fong
 *
 */
public class Wall {
	
	/**
	 * The wall's current top-left x position
	 */
	private int x;
	
	/**
	 * The wall's current top-left y position
	 */
	private int y;
	
	/**
	 * The wall's size (width & height)
	 */
	private int size;
	
	/**
	 * The player that the wall is related to (1-4)
	 */
	private int player;
	
	/**
	 * True if the wall has been hit or HP reduced to 0
	 */
	private boolean isDestroyed;
	
	/**
	 * The colour of the wall relative to the player
	 */
	private Color c;

	/**
	 * Default constructor of the wall class. Initialises all private attributes to 0 and sets
	 * its colour to transparent.
	 */
	public Wall(){
		this.x = 0;
		this.y = 0;
		this.size = 0;
		this.isDestroyed = false;
		this.player = 0;
		this.c = Color.TRANSPARENT;
	}

	/**
	 * Alternative constructor taking inputs. Sets the size of the wall to 24 and sets the colour
	 * according to the player given.
	 * @param x - the x position of the wall
	 * @param y - the y position of the wall
	 * @param player - the player related to this wall
	 */
	public Wall(int x, int y, int player) {
		this.x = x;
		this.y = y;
		this.isDestroyed = false;
		this.size = 32;
		this.player = player;

		switch (player) {
		case 0: this.c = Color.BLACK;
		break;
		case 1: this.c = Color.web("782121");
		break;
		case 2: this.c = Color.web("006680");
		break;
		case 3: this.c = Color.web("d45500");
		break;
		case 4: this.c = Color.web("225500");
		break;
		}
	}

	/**
	 *  Set the horizontal position of the wall to the given value.
	 * @param x
	 */
	public void setXPos(int x) {
		this.x = x;
	}
	
	/**
	 *  Set the vertical position of the wall to the given value.
	 * @param y
	 */
	public void setYPos(int y) {
		this.y = y;
	}

	/**
	 * Determines if this wall has been destroyed.
	 *
	 * @return true if the ball has collided with this wall. Otherwise, return false.
	 */
	public boolean isDestroyed() {
		return isDestroyed;
	}
	
	public int getPlayer(){
		return player;
	}

	/**
	 * Sets the boolean isDestroyed to true
	 */
	public void destroy() {
		isDestroyed = true;
	}

	/**
	 * Sets the boolean isDestroyed to false 
	 */
	public void reset() {
		isDestroyed = false;
	}

	/**
	 * @return the top-left x position of the wall
	 */
	public int getXPos() {
		return x;
	}

	/**
	 * @return the top-left y position of the wall
	 */
	public int getYPos() {
		return y;
	}

	/**
	 * @return the size of the wall
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Draws the wall to the canvas that the GraphicsContext is related to
	 * according to its current colour, x position, y position and size.
	 * @param gc
	 */
	public void draw(GraphicsContext gc) {
		gc.setFill(c);
		gc.fillRect(x, y, size, size);
	}

	/**
	 * @return a rectangle with the wall's, x and y position and width and height
	 * related to its size.
	 */
	public Rectangle getHitBox() {
		return new Rectangle(x, y, size, size);
	}

}
