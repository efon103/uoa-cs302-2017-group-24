package entity;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.GameWindow;

/**
 * The Warlord class holds information about a specific warlord playing dotL, including
 * their x position, y position, score, size, death status, winning status and colour.
 * @author William Chao and Eugene Fong
 *
 */
public class Warlord {
	
	/**
	 * The warlord's current top-left x position
	 */
	private int xPos;
	
	/**
	 * The warlord's current top-left y position
	 */
	private int yPos;
	
	/**
	 * The warlord's current score
	 */
	private int score;
	
	/**
	 * The warlord's size
	 */
	public static final int SIZE = 100;
	
	/**
	 * Is true if the warlord is dead
	 */
	private boolean isDead;
	
	/**
	 * The colour of the warlord
	 */
	private Color c;
	
	/**
	 * Default constructor for the warlord class, initialises all private attributes
	 * to 0 and sets its colour to transparent
	 */
	public Warlord() {
		xPos = 0;
		yPos = 0;
		score = 0;
		isDead = false;
		c = Color.TRANSPARENT;
	}
	
    /**
     *  Set the horizontal position of the warlord to the given value if it is
     *  within range.
     * @param x
     */
    public void setXPos(int x) {
    	if(!((x < 0) || (x >= GameWindow.WIDTH-SIZE))) {
    		xPos = x;
    	}
    }

    /**
     *  Set the vertical position of the warlord to the given value if it is
     *  within range.
     * @param y
     */
    public void setYPos(int y) {
    	if(!((y < 0) || (y >= GameWindow.WIDTH-SIZE))) {
    		yPos = y;
    	}
    }

    /**
     * @return true if a ball has collided with this warlord. Otherwise, return false.
     */
    public boolean isDead() {
    	return isDead;
    }
    
    /**
     * @return the warlord's score depending on how many times it has won in a game's instance.
     */
    public int getScore() {
    	return score;
    }
    
    /**
     * Sets the boolean hasWon to true and increases the warlord's score by 1.
     */
    public void win(){
		score++;
    }
    
    /**
     * Sets the boolean isDead to true
     */
	public void die() {
		isDead = true;
	}
	
	/**
	 * Resets the boolean isDead and hasWon to false.
	 */
	public void revive() {
		isDead = false;
	}
	
	/**
	 * Sets the warlord's x and y position and colour depending on the player given.
	 * @param player - the player's number (1-4)
	 */
	public void setPlayer(int player) {
		switch(player) {
		case 1: c = Color.web("c83737");
				setXPos(125-SIZE/2);
				setYPos(90-SIZE/2);
				break;
		case 2: c = Color.web("00aad4");
				setXPos(125-SIZE/2);
				setYPos(677-SIZE/2);
				break;
		case 3: c = Color.web("ff6600");
				setXPos(900-SIZE/2);
				setYPos(90-SIZE/2);
				break;
		case 4: c = Color.web("008000");
				setXPos(900-SIZE/2);
				setYPos(677-SIZE/2);
				break;
		}
	}
	
	/**
	 * @return the colour of the warlord
	 */
	public Color getColour() {
		return c;
	}
	
	/**
	 * Draws the warlord to the canvas given the GraphicsContext that is related to the canvas.
	 * Sets the GraphicsContext's fill colour to the warlord's colour and fills an oval at the
	 * warlord's x and y position with the width/height given by the warlord's size.
	 * @param gc
	 */
	public void draw(GraphicsContext gc) {
		gc.setFill(c);
		gc.fillOval(xPos, yPos, SIZE, SIZE);
	}
	
	/**
	 * @return a rectangle at the warlord's x-y position with width and height given by the warlord's size.
	 */
	public Rectangle getHitBox() {
		return new Rectangle(xPos, yPos, SIZE, SIZE);
	}

}
