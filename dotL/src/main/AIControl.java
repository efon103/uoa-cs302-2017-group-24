package main;

import entity.Ball;
import entity.Paddle;

/**
 * The AIControl controls the computer players in the default game mode. It's purpose is to
 * detect the position of the ball relative to the paddle and move to the appropriate region.
 * There are three regions the AI operates, the vertical only region, the horizontal only
 * region and the corner edge closest to the centre.
 * @author William Chao and Eugene Fong
 *
 */

public class AIControl {

	/**
	 * x position of ball
	 */
	private double xBall;
	
	/**
	 * y position of ball
	 */
	private double yBall;
	
	/**
	 * the player the AI is controlling
	 */
	private int player;
	
	/**
	 * skill level of AI
	 */
	private int skill;
	
	/**
	 * Command to be used by the paddle (i.e. LEFT or RIGHT)
	 */
	private String command;
	
	/**
	 * Timer creates the delay according to skill level
	 */
	private Timer timer;

	/**
	 * AIControl constructor, player indicates which corner the AI is working from and skill is for skill level.
	 * A timer is created on the creation of AI to ensure that the AI performance remains consistent regardless
	 * of round.
	 * @param player
	 * @param skill
	 */
	public AIControl(int player, int skill){
		this.player = player;
		this.skill = skill;
		
		timer = new Timer();
		timer.setRandom(10-skill);
	}

	/**
	 * Returns true is skill level is not zero (i.e. a human player)
	 */
	public boolean isActive(){
		if (skill > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Sets skill level of AI
	 */
	public void setSkill(int skill) {
		this.skill = skill;
	}

	/**
	 * Tick represents a single process completed by the game in one loop
	 */
	public void tick(Ball b, Paddle p){
		
		xBall = b.getXPos() - b.getSize()/2; //get x centre of ball
		yBall = b.getYPos() - b.getSize()/2; //get y centre of ball

		//if time delay of AI skill meets criteria
		if (timer.checkTimer()){
			
			timer.reset();	//reset the timer for the next action
			
			switch (player){
			
			//player 1
			case 1:
				//if ball is in horizontal paddle area
				if ((xBall >= 0) && (xBall < DefaultMode.size*DefaultMode.cols)){
					//if paddles is not in horizontal area, move paddle to horizontal area
					if (p.getPos() > 272-p.getWidth()/2){
						command = "LEFT";
					}
					else{
						//seek paddle towards x position of ball, otherwise halt
						if (xBall > (p.getPos()+p.getWidth()/2)){
							command = "RIGHT";
						}
						else if (xBall < (p.getPos()+p.getWidth()/2)){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in vertical paddle area
				else if ((yBall >= 0) && (yBall < DefaultMode.size*DefaultMode.rows)){
					//if paddles is not in vertical area, move paddle to vertical area
					if (p.getPos() < 272+p.getWidth()/2){
						command = "RIGHT";
					}
					else{
						//seek paddle towards y position of ball, otherwise halt
						if (yBall < (p.getPos()-256)+p.getWidth()){
							command = "RIGHT";
						}
						else if (yBall > (p.getPos()-256)+p.getWidth()){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in neither area, for example diagonal/near the centre, the paddles moves to a defensive position on the corner 
				else if ((yBall > DefaultMode.size*DefaultMode.rows) && (xBall > DefaultMode.size*DefaultMode.cols)){
					if (p.getPos() < 272-48){
						command = "RIGHT";
					}
					else if (p.getPos() > 272+48){
						command = "LEFT";
					}
					else{
						command = "HALT";
					}
				}
				break;
				
			//player 2
			case 2:
				//if ball is in horizontal paddle area
				if ((xBall >= 0) && (xBall < DefaultMode.size*DefaultMode.cols)){
					//if paddles is not in horizontal area, move paddle to horizontal area
					if (p.getPos() > 272-p.getWidth()/2){
						command = "LEFT";
					}
					else{
						//seek paddle towards x position of ball, otherwise halt
						if (xBall > (p.getPos()+p.getWidth()/2)){
							command = "RIGHT";
						}
						else if (xBall < (p.getPos()+p.getWidth()/2)){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in vertical paddle area
				else if ((yBall > GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows) && (yBall <= GameWindow.HEIGHT)){
					//if paddles is not in vertical area, move paddle to vertical area
					if (p.getPos() < 272+p.getWidth()/2){
						command = "RIGHT";
					}
					else{
						//seek paddle towards y position of ball, otherwise halt
						if (yBall > GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows+(p.getPos()-256-p.getWidth())){
							command = "RIGHT";
						}
						else if (yBall < GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows+(p.getPos()-256-p.getWidth())){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in neither area, for example diagonal/near the centre, the paddles moves to a defensive position on the corner
				else if ((yBall < GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows) && (xBall > DefaultMode.size*DefaultMode.cols)){
					if (p.getPos() < 272-48){
						command = "RIGHT";
					}
					else if (p.getPos() > 272+48){
						command = "LEFT";
					}
					else{
						command = "HALT";
					}
				}
				break;
			case 3:
				//if ball is in horizontal paddle area
				if ((xBall <= GameWindow.WIDTH) && (xBall > GameWindow.WIDTH-DefaultMode.size*DefaultMode.cols)){
					//if paddles is not in horizontal area, move paddle to horizontal area
					if (p.getPos() > 272-p.getWidth()/2){
						command = "RIGHT";
					}
					else{
						//seek paddle towards x position of ball, otherwise halt
						if (xBall > GameWindow.WIDTH-(p.getPos()+p.getWidth()/2)){
							command = "RIGHT";
						}
						else if (xBall < GameWindow.WIDTH-(p.getPos()+p.getWidth()/2)){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in vertical paddle area
				else if ((yBall >= 0) && (yBall < DefaultMode.size*DefaultMode.rows)){
					//if paddles is not in vertical area, move paddle to vertical area
					if (p.getPos() < 272+p.getWidth()/2){
						command = "LEFT";
					}
					else{
						//seek paddle towards y position of ball, otherwise halt
						if (yBall < (p.getPos()-256+p.getWidth())){
							command = "LEFT";
						}
						else if (yBall > (p.getPos()-256)+p.getWidth()){
							command = "RIGHT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in neither area, for example diagonal/near the centre, the paddles moves to a defensive position on the corner
				else if ((yBall > DefaultMode.size*DefaultMode.rows) && (xBall < GameWindow.WIDTH-DefaultMode.size*DefaultMode.cols)){
					if (p.getPos() < 272-48){
						command = "LEFT";
					}
					else if (p.getPos() > 272+48){
						command = "RIGHT";
					}
					else{
						command = "HALT";
					}
				}
				break;	
			case 4:
				//if ball is in horizontal paddle area
				if ((xBall <= GameWindow.WIDTH) && (xBall > GameWindow.WIDTH-DefaultMode.size*DefaultMode.cols)){
					//if paddles is not in horizontal area, move paddle to horizontal area
					if (p.getPos() > 272-p.getWidth()/2){
						command = "RIGHT";
					}
					else{
						//seek paddle towards x position of ball, otherwise halt
						if (xBall > GameWindow.WIDTH-(p.getPos()+p.getWidth()/2)){
							command = "RIGHT";
						}
						else if (xBall < GameWindow.WIDTH-(p.getPos()+p.getWidth()/2)){
							command = "LEFT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in vertical paddle area
				else if ((yBall >= GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows) && (yBall < GameWindow.HEIGHT)){
					//if paddles is not in vertical area, move paddle to vertical area
					if (p.getPos() < 272+p.getWidth()/2){
						command = "LEFT";
					}
					else{
						//seek paddle towards y position of ball, otherwise halt
						if (yBall > GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows+(p.getPos()-256-p.getWidth())){
							command = "LEFT";
						}
						else if (yBall < GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows+(p.getPos()-256-p.getWidth())){
							command = "RIGHT";
						}
						else {
							command = "HALT";
						}
					}
				}
				//if ball is in neither area, for example diagonal/near the centre, the paddles moves to a defensive position on the corner
				else if ((yBall < GameWindow.HEIGHT-DefaultMode.size*DefaultMode.rows) && (xBall < GameWindow.WIDTH-DefaultMode.size*DefaultMode.cols)){
					if (p.getPos() < 272-48){
						command = "LEFT";
					}
					else if (p.getPos() > 272+48){
						command = "RIGHT";
					}
					else{
						command = "HALT";
					}
				}
				break;
			}
		}
	}

	/**
	 * getCommand returns the last command issued during a tick
	 * @return command
	 */
	public String getCommand(){
		return command;
	}
}
