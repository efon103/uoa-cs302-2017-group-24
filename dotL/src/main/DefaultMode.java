package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import entity.Ball;
import entity.Paddle;
import entity.Wall;
import entity.Warlord;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * The DefaultMode class is the default mode of the game. It controls all logic
 * and entities created in the game. It can be extended for other game modes in the future.
 * @author William Chao and Eugene Fong
 *
 */
public class DefaultMode {

	/**
	 * The game's ball
	 */
	private Ball ball = new Ball();

	/**
	 * The game's paddles
	 */
	private List<Paddle> paddles = new ArrayList<Paddle>();

	/**
	 * The game's walls
	 */
	private List<Wall> walls  = new ArrayList<Wall>();

	/**
	 * The game's warlords
	 */
	private List<Warlord> warlords = new ArrayList<Warlord>();

	/**
	 * Number of players still alive
	 */
	private int playersAlive;

	/**
	 * True if nobody has won yet or if timer is still counting down
	 */
	private boolean gameRunning = false;

	/**
	 * True if waiting for game to start and do 3 second countdown
	 */
	private boolean preGame = false;
	
	/**
	 * True if game screen is active else false
	 */
	private boolean active = false;

	/**
	 * The layers of walls for each warlord
	 */
	public static int layers = 3;

	/**
	 * The max number of rows for the walls
	 */
	public static int rows = 8;

	/**
	 * The max number of columns for the walls
	 */
	public static int cols = 10; 

	/**
	 * The time the last input was pressed
	 */
	private long lastPress = 0;

	/**
	 * The amount of walls each player has
	 */
	private int[] playerWalls = new int[4];
	
	/**
	 * The highest number of walls of the players alive
	 */
	private int mostWalls = 0;
	
	/**
	 * Used to count how many walls a player currently has
	 */
	private int wallCount;

	/**
	 * The player who destroyed all three other warlords
	 */
	private int winner;

	/**
	 * True if game is paused else false
	 */
	private boolean paused = false;
	
	/**
	 * The timer for the game controls three second countdown and two minute time limit
	 */
	private GameTimer timer;
	
	/**
	 * The primary colour of the game
	 */
	private Color defaultPrimary;
	
	/**
	 * The background colour of the game
	 */
	private Color defaultBackground;
	
	/**
	 * The opacity of the base
	 */
	private double opacity;

	/**
	 * The theme of the game - light/dark
	 */
	private String theme;
	
	/**
	 * AI for player one
	 */
	private AIControl player1;
	
	/**
	 * AI for player two
	 */
	private AIControl player2;
	
	/**
	 * AI for player three
	 */
	private AIControl player3;
	
	/**
	 * AI for player four
	 */
	private AIControl player4;
	
	/**
	 * Is 0 if human else player is AI of difficulty level 1-10. Index of humans[] is related to the player.
	 */
	private int[] humans;
	
	/**
	 * True if there is at least one human in the game else false
	 */
	private boolean humanInGame;

	/**
	 * True if escape has been pressed else false
	 */
	private boolean escActive;

	/**
	 * The size of the walls
	 */
	public static int size = 32;
	
	/**
	 * Default constructor of DefaultMode creates a new GameTimer and initialises all entities and variables.
	 */
	public DefaultMode(){
		timer = new GameTimer();
		escActive = false;
		winner = 0;
		
		//Create 4 warlords and paddles for players 1-4
		for(int i = 0; i < 4; i++) {
			warlords.add(new Warlord());
			paddles.add(new Paddle(0, 0, i+1));
			warlords.get(i).setPlayer(i+1);
			playersAlive++;
		}

		wallGen();

		ball = new Ball();
		timer.beginGame();

		player1 = new AIControl(1,0);
		player2 = new AIControl(2,0);
		player3 = new AIControl(3,0);
		player4 = new AIControl(4,0);
		
	}

	/**
	 * Sets the primary and background colours to the respective theme
	 * @param theme - Light/Dark
	 */
	public void setTheme(String theme){
		this.theme = theme;

		if (theme == "Light" ){
			defaultPrimary = Color.BLACK;
			defaultBackground = Color.web("f9f9f9");
			opacity = 1;
		}
		else if (theme == "Dark"){
			defaultPrimary = Color.web("f3f3f3");
			defaultBackground = Color.web("404040");
			opacity = 0.2;
		}
	}

	/**
	 * Resets all players' warlords, paddles, walls and sets the timer to countdown from 3
	 * down to 1 bringing the game into the preGame state
	 */
	public void reset(){
		preGame = true;
		active = true;
		ball.setSize(0);
		
		Arrays.fill(playerWalls, 0);
		playersAlive = warlords.size();
		for(int i = 0; i < warlords.size(); i++) {
			warlords.get(i).revive();
		}
		
		for(int i = 0; i < paddles.size(); i++) {
			paddles.get(i).reset();
		}

		for(int i = 0; i < walls.size(); i++) {
			walls.get(i).reset();
		}

		timer.beginGame();
	}
	/**
	 * Sets the timer to two minutes and sets the ball's x, y position,
	 * gives the ball a random x and y velocity and sets its colour and size.
	 * Sets preGame to false to get out of the three second countdown and gameRunning
	 * to true to allow objects to move.
	 */
	public void init() {
		timer.setTimeRemaining(120);

		ball.setXPos(GameWindow.WIDTH/2-12);
		ball.setYPos(GameWindow.HEIGHT/2-12);

		//Get random velocity (between 6 and 10)/direction
		int xDir = ThreadLocalRandom.current().nextInt(0,2); 
		int yDir = ThreadLocalRandom.current().nextInt(0,2);
		int dx = ThreadLocalRandom.current().nextInt(6,11);
		int dy = ThreadLocalRandom.current().nextInt(6,11);

		//if xDir is 1 then x direction is left
		if (xDir == 1) {
			dx = -dx;
		}
		
		//if yDir is 1 then y direction is up
		if (yDir == 1) {
			dy = -dy;
		}

		ball.setXVelocity(dx);
		ball.setYVelocity(dy);

		ball.setColour(defaultPrimary);
		ball.setSize(24);

		preGame = false;
		gameRunning = true;
		active = true;
	}
	
	/**
	 * @return true if game screen is currently active
	 */
	public boolean isActive(){
		return active;
	}

	/**
	 * Sets the AI for non-human players
	 * @param humans - size four for four players. If 0 then it's a player else it is an AI.
	 */
	public void setAI(int[] humans){
		this.humans = humans;
		humanInGame = false;
		player1 = new AIControl(1,humans[0]);
		player2 = new AIControl(2,humans[1]);
		player3 = new AIControl(3,humans[2]);
		player4 = new AIControl(4,humans[3]);

		for(int i = 0; i < humans.length; i++){
			if (humans[i] == 0) {
				humanInGame = true;
			}
		}
	}

	/**
	 * Updates game data. Checks for global inputs such as esc, p, pgDown and enter.
	 * If the game is currently running it checks if the timer has reached 2 minutes
	 * then ends the game otherwise, it checks if the ball will collide with the paddles
	 * that are still alive then if it will collide with any walls, then if it will collide
	 * with any warlords. If there's only one player left, it ends the game. It then updates
	 * the ball's position and updates the paddle's position depending on the keys pressed.
	 * If the game is in preGame state, it checks if timer has reached three seconds then calls
	 * init() to initialise the game.
	 */
	public void tick(){
		checkKeys();
		if(gameRunning) {
			if (timer.checkTimer()) {
				endGame();
			} else {
				boolean hit = false; 
				if (!hit) {
					//Check if paddle been hit first as it is the outermost object
					for (int i = 0; i < paddles.size(); i++) {
						if(!warlords.get(i).isDead()) {
							if(ball.registerHit(paddles.get(i).getHitBoxVert()) || ball.registerHit(paddles.get(i).getHitBoxHori())) {
								//play the bounce sound and break out of collision check to update ball/paddle positions
								GameController.playSound("/a.mp3");
								hit = true;
								break;
							}
						}
					}		

					//Check if wall has been hit (loops through outer most walls to inner walls)
					for (int i = 0; i < walls.size(); i++) {
						if(!walls.get(i).isDestroyed()) {
							if(ball.registerHit(walls.get(i).getHitBox())) {
								//destroy the wall if it gets hit and play destruction sound
								GameController.playSound("/b.wav");
								walls.get(i).destroy();
								hit = true;
								break;
							}
						}
					}

					//Check if warlord been hit
					for (int i = 0; i < warlords.size(); i++) {
						if (!warlords.get(i).isDead()) {
							if(ball.registerHit(warlords.get(i).getHitBox())) {	
								//kill the warlord if it gets hit, remove 1 player alive and play destruction sound
								GameController.playSound("/b.wav");
								warlords.get(i).die();
								playersAlive--;
								hit = true;
								break;
							}
						}
					}
				}

				//If the game is still running and there's only one person then the game has ended.
				if (playersAlive == 1) {
					endGame();
				}

				//Update balls position and then sense input for paddles.
				ball.update();
				updateUserInput();
				
				//Updates the AI's input if any AI is active
				if(player1.isActive()){
					player1.tick(ball, paddles.get(0));
					paddles.get(0).keyPressed(player1.getCommand());
				}
				if(player2.isActive()){
					player2.tick(ball, paddles.get(1));
					paddles.get(1).keyPressed(player2.getCommand());
				}
				if(player3.isActive()){
					player3.tick(ball, paddles.get(2));
					paddles.get(2).keyPressed(player3.getCommand());
				}
				if(player4.isActive()){
					player4.tick(ball, paddles.get(3));
					paddles.get(3).keyPressed(player4.getCommand());
				}
			}
		} else if (preGame == true) {
			//when timer reaches three seconds, initialises the game.
			if (timer.checkTimer()) {
				init();
			}
		}
	}
	
	/**
	 * Draws the background of the game, text displayed on game screen, balls, warlords,
	 * walls, paddles and bases. 
	 * @param gc - the graphics context of the canvas
	 */
	public void draw(GraphicsContext gc){
		//Background
		gc.setFill(defaultBackground);
		gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);

		//AI only game should display text for Demo mode
		if (!humanInGame){
			gc.setFill(defaultPrimary);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("DEMO MODE\n Press ESC to exit", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/3+64);
		}
		
		//Draws/Updates the timer according to the theme's colour if esc hasn't been pressed
		if (!escActive){
			timer.setTheme(theme);
			timer.draw(gc);
		}
		
		//Draws the bases of each player
		drawBases(gc);
		
		//Draws the scores of each player
		drawScoreboard(gc);

		//draw ball with primary colour
		gc.setFill(defaultPrimary);
		ball.draw(gc);
		
		//Draws paddles, warlords, walls active
		drawActiveObjects(gc);
		
		//If escape is pressed it draws text to confirm if the player wants to quit or unpause the game
		if (escActive){
			gc.setFill(Color.web("f9f9f9", 0.8));
			gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);
			gc.setFill(Color.BLACK);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 36));
			gc.fillText("MENU", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/5);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("Press ESC to exit\n\n Press P to unpause", GameWindow.WIDTH/2, 3*GameWindow.HEIGHT/5);
		}
	}
	
	/**
	 * If players 1-4 are humans then it will read input from the keyboard and update
	 * the paddle's positions accordingly.
	 */
	private void updateUserInput() {

		if (humans[0] == 0) {

			if (GameWindow.getActiveKeys().contains("DIGIT1")) {
				paddles.get(0).keyPressed("LEFT");
			}

			if (GameWindow.getActiveKeys().contains("DIGIT3")) {
				paddles.get(0).keyPressed("RIGHT");
			}
		}

		if (humans[1] == 0) {
			if (GameWindow.getActiveKeys().contains("C")) {
				paddles.get(1).keyPressed("LEFT");
			}

			if (GameWindow.getActiveKeys().contains("B")) {
				paddles.get(1).keyPressed("RIGHT");
			}
		}

		if (humans[2] == 0) {
			if (GameWindow.getActiveKeys().contains("DIGIT7")) {
				paddles.get(2).keyPressed("LEFT");
			}

			if (GameWindow.getActiveKeys().contains("DIGIT9")) {
				paddles.get(2).keyPressed("RIGHT");
			}
		}

		if (humans[3] == 0) {
			if (GameWindow.getActiveKeys().contains("LEFT")) {
				paddles.get(3).keyPressed("LEFT");
			}

			if (GameWindow.getActiveKeys().contains("RIGHT")) {
				paddles.get(3).keyPressed("RIGHT");
			}
		}

	}

	/**
	 * Checks for global inputs such as P for pause, ESC for menu, ENTER to restart
	 * and Pg_Down to end game
	 */
	public void checkKeys() {

		if (GameWindow.getActiveKeys().contains("P")) {
			//Used for debouncing so that user has to press for 250ms before it can be registered again
			if ((System.currentTimeMillis()-lastPress > 250)) {
				if (preGame) {
					if (paused) {
						paused = false;
						lastPress = System.currentTimeMillis();
						timer.startTimer();
					} else {
						paused = true;
						lastPress = System.currentTimeMillis();
						timer.pauseTimer();
					}
				} else if (gameRunning && !preGame){
					gameRunning = !gameRunning;
					lastPress = System.currentTimeMillis();
					timer.pauseTimer();
				} else if (!gameRunning && !preGame) {
					gameRunning = !gameRunning;
					lastPress = System.currentTimeMillis();
					escActive = false;
					timer.startTimer();
				}
			}
		}

		if (GameWindow.getActiveKeys().contains("ENTER")) {
			//Should only be pressed when the game is finished not during countdown or while game is running
			if ((System.currentTimeMillis()-lastPress > 250) && (gameRunning == false) && preGame == (false)) {
				lastPress = System.currentTimeMillis();
				reset();
			}
		}
		
		//sets time remaining to 0 to end the game
		if (GameWindow.getActiveKeys().contains("PAGE_DOWN")) {
			if (System.currentTimeMillis()-lastPress > 250){
				lastPress = System.currentTimeMillis();
				timer.setTimeRemaining(0);
			}
		}
		
		//Brings up ESCAPE screen while the game is running and escape is pressed
		if (GameWindow.getActiveKeys().contains("ESCAPE")) {
			if (System.currentTimeMillis()-lastPress > 250){
				lastPress = System.currentTimeMillis();
				if (!escActive && gameRunning){
					gameRunning = false;
					escActive = true;
					timer.pauseTimer();
				} else if (escActive){
					//Quits game if esc is pressed again while in ESCAPE menu
					active = false;
				}
			}
		}
	}
	
	/**
	 * Stops the game from running, checks for the winners by counting number of walls
	 * for players still alive, increases their scores and if only one player is alive,
	 * then the winner is that person.
	 */
	private void endGame(){
		
		//Only one player alive, don't need to count walls
		if(playersAlive == 1) {
			for (int i = 0; i < warlords.size(); i++) {
				if(!warlords.get(i).isDead()) {
					warlords.get(i).win();
					winner = i+1;
				}
			}
		} else {
			wallCount = 0;
			mostWalls = 0;
			for (int i = 0; i < warlords.size(); i++) {
				//Only check walls of warlords alive
				if(!warlords.get(i).isDead()) {
					wallCount = 0;
					for (int j = 0; j < walls.size(); j++){
						if ((!walls.get(j).isDestroyed()) && (walls.get(j).getPlayer()-1 == i)){
							//increase wall count for warlord if that wall is still alive and related to the warlord is still alive
							wallCount++;
						}
					}
					
					//sets player 1-4's walls to wallcount
					playerWalls[i] = wallCount;
					
					//Maximum check
					if (wallCount > mostWalls){
						mostWalls = wallCount;
					}
				}
			}
	
			for (int k = 0; k < warlords.size(); k++){
				if (playerWalls[k] == mostWalls){
					//Warlord with most walls wins!
					warlords.get(k).win();
					winner = k+1;
				}
			}
		}
		gameRunning = false;
	}
	
	/**
	 * Generates walls for all players forming the outermost layer first going through to the
	 * innermost layer last.
	 */
	private void wallGen(){
		for (int i = 0; i < layers; i++) {
			for(int j = 0; j < cols-i; j++) {
				//32 is the wall size
				walls.add(new Wall(j*(size), 224-(i*size),1));
				walls.add(new Wall(j*(size), 512+(i*size), 2));
				walls.add(new Wall(GameWindow.WIDTH-size-(j*size),224-(i*size),3));
				walls.add(new Wall(GameWindow.WIDTH-size-(j*size),512+(i*size),4));
				//when it reaches the last column, we then go down the rows
				if (j == cols-1-i) {
					for(int k = i+1; k < rows; k++) {				
						walls.add(new Wall(j*(size),224-(k*size), 1));
						walls.add(new Wall(j*(size), 512+(k*size), 2));
						walls.add(new Wall(GameWindow.WIDTH-size-(j*size),224-(k*size),3));
						walls.add(new Wall(GameWindow.WIDTH-size-(j*size),512+(k*size),4));
					}
				}
			}
		}
	}
	
	/**
	 * Draws a transparent layer across all four players' base
	 * @param gc - the graphicsContext of the canvas
	 */
	private void drawBases(GraphicsContext gc){
		//set game base colours
		//base of player 1
		gc.setFill(Color.web("efc4c4", opacity));
		gc.fillRect(0, 0, GameWindow.WIDTH/32*10, GameWindow.HEIGHT/3);
		//base of player 2
		gc.setFill(Color.web("d5f6ff", opacity));
		gc.fillRect(0, 2*GameWindow.HEIGHT/3, GameWindow.WIDTH/32*10, GameWindow.HEIGHT/3);
		//base of player 3
		gc.setFill(Color.web("ffd5b7", opacity));
		gc.fillRect(GameWindow.WIDTH - GameWindow.WIDTH/32*10, 0, GameWindow.WIDTH/32*10, GameWindow.HEIGHT/3);
		//base of player 4
		gc.setFill(Color.web("e3f4d7", opacity));
		gc.fillRect(GameWindow.WIDTH -  GameWindow.WIDTH/32*10, 2*GameWindow.HEIGHT/3, GameWindow.WIDTH/32*10, GameWindow.HEIGHT/3);
	}
	
	/**
	 * Draws the scoreboard for all four players depending on their current score
	 * @param gc - the graphicsContext of the canvas
	 */
	private void drawScoreboard(GraphicsContext gc) {
		//draw scoreboard
		gc.setFill(warlords.get(0).getColour());
		gc.setFont(Font.font("Arial", 30));
		gc.fillText(Integer.toString(warlords.get(0).getScore()), 375, 325);

		gc.setFill(warlords.get(1).getColour());
		gc.setFont(Font.font("Arial", 30));
		gc.fillText(Integer.toString(warlords.get(1).getScore()), 375, 450);

		gc.setFill(warlords.get(2).getColour());
		gc.setFont(Font.font("Arial", 30));
		gc.fillText(Integer.toString(warlords.get(2).getScore()), 650, 325);

		gc.setFill(warlords.get(3).getColour());
		gc.setFont(Font.font("Arial", 30));
		gc.fillText(Integer.toString(warlords.get(3).getScore()), 650, 450);
	}

	/**
	 * Draws the walls, warlord and paddles that are still alive in the game.
	 * @param gc - the graphicsContext of the canvas
	 */
	private void drawActiveObjects(GraphicsContext gc){
		//draw active walls
		for (int i = 0; i < walls.size(); i++) {
			if(!walls.get(i).isDestroyed()) {
				walls.get(i).draw(gc);
			}
		}

		//draw active paddles and warlords
		for (int i = 0; i < 4; i++) {
			if(!warlords.get(i).isDead()) {
				paddles.get(i).draw(gc);
				warlords.get(i).draw(gc);
			}
		}
	}
	
	/**
	 * @return true if game is currently running else false
	 */
	public boolean isGameRunning() {
		return gameRunning;
	}

	/**
	 * @return the winner, player 1, 2, 3 or 4
	 */
	public int getWinner() {
		return winner;
	}

}
