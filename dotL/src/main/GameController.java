package main;


import javafx.scene.Group;

import javafx.scene.canvas.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * The GameController class controls all screens such as the splash, menu and game screen
 * and plays audio. It controls which screen to tick and which screen it should draw to 
 * its canvas.
 * @author William Chao and Eugene Fong
 *
 */
public class GameController {

	/**
	 * The GraphicsContext of the game which paints the canvas
	 */
	private GraphicsContext gc;

	/**
	 * The canvas of the game it draws on
	 */
	private Canvas canvas;

	/**
	 * The game screen
	 */
	private static DefaultMode game = new DefaultMode();

	/**
	 * The splash screen
	 */
	private SplashScreen splash;

	/**
	 * The menu screen
	 */
	private static MainMenu menu;

	/**
	 * Constructor initialises the canvas, GraphicsContext, splash screen and
	 * the menu screen.
	 * @param root - the root group node of the game window
	 */
	public GameController(Group root) {
		canvas = new Canvas(GameWindow.WIDTH, GameWindow.HEIGHT);
		gc = canvas.getGraphicsContext2D();
		root.getChildren().add(canvas);

		splash = new SplashScreen();
		menu = new MainMenu();
	}

	/**
	 * Runs game logic once per tick which updates either the splash, 
	 * menu or game screen depending on which one is currently active.
	 */
	public void tick() {
		
		if (splash.isSplashScreenRunning()){
			splash.tick();
		} else {
			menu.start();
		}

		if (menu.isMainMenuRunning()){
			menu.tick();
		}
		
		if (game.isActive()){
			game.tick();
		} else if (!menu.isMainMenuRunning() && !splash.isSplashScreenRunning() && !game.isActive()){
			if (menu.getGameMode() == 1){
				game = new DefaultMode();
				game.setTheme(menu.getTheme());
				game.setAI(menu.getHumans());
				game.reset();
			}
		}
	}

	/**
	 * Draws over the canvas of the window with the GraphicsContext depending
	 * on which screen is currently active.
	 */
	public void draw() {

		if (game.isActive()){
			game.draw(gc);
		} else if (menu.isMainMenuRunning()){
			menu.draw(gc);
		} else if (splash.isSplashScreenRunning()){
			splash.draw(gc);
		}
	}
	
	/**
	 * Plays the audio given by the file using a MediaPlayer.
	 * @param file - the file name i.e. "/bounce.wav"
	 */
	public static void playSound(String file) {
		
		try {
			//Gets the full URI of the file and makes it a media
			//Media audio = new Media(DefaultMode.class.getResourceAsStream(file).toString());
			//Media audio = new Media(DefaultMode.class.getResource(file).toString());
			Media audio = new Media(DefaultMode.class.getResource(file).toURI().toString());
			//MediaPlayer takes the media above and plays it.
			MediaPlayer mediaPlayer = new MediaPlayer(audio);
			mediaPlayer.setVolume(menu.getVolume());
			mediaPlayer.play();
		} catch (Exception e) {
			//Used so if sound does not work, it does not crash the game.
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @return true if game screen is running
	 */
	public static boolean getGameRunning() {
		return game.isActive();
	}
	
	/**
	 * @return the game that has been initialised by the controller
	 */
	public static DefaultMode getGame() {
		return game;
	}
	
}
