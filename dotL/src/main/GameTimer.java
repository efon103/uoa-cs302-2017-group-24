package main;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * The GameTimer controls all the timing functionality during gameplay
 * @author William Chao and Eugene Fong
 *
 */
public class GameTimer {

	/**
	 * The current time of the game, related to the system's time since start
	 */
	private long currentTime = 0;

	/**
	 * The time limit for each game in seconds (default 120)
	 */
	private long targetTime = 0;

	/**
	 * Seconds left
	 */
	private long sec = 0;

	/**
	 * Minutes left
	 */
	private long mins = 0;

	/**
	 * Time difference between currentTime and targetTime.
	 */
	private long delta;

	/**
	 * Time remaining before the game ends, used to pause and unpause timer.
	 */
	private long timeRemaining;

	/**
	 * Paused state. True if game is paused, false otherwise.
	 */
	private boolean paused = false;

	/**
	 * Timer mode, i.e. pre game countdown or 2 minute game timer.
	 */
	private int mode;
	
	/**
	 * textColor store the colour of the text
	 */
	private Color textColor;

	/**
	 * Time remaining displayed as M:SS
	 */
	private String timeLeft;

	public GameTimer(){

	}

	/**
	 * beginGame starts the 3 second timer for the pre-game countdown
	 */
	public void beginGame(){
		mode = 1;
		targetTime = System.currentTimeMillis()+4000;
		currentTime = System.currentTimeMillis();
		paused = false;
	}

	/**
	 * Sets the timer to count down from a given input in seconds
	 */
	public void setTimeRemaining(int seconds){
		mode = 2;
		targetTime = System.currentTimeMillis()+seconds*1000;
		currentTime = System.currentTimeMillis();
		paused = false;
	}

	/**
	 * Pauses the timer by remembering the point in time the count is paused
	 */
	public void pauseTimer(){
		timeRemaining = targetTime - currentTime;
		targetTime = -1;
		paused = true;
	}

	/**
	 * Resumes the timer
	 */
	public void startTimer(){
		currentTime = System.currentTimeMillis();	
		targetTime = currentTime + timeRemaining;
		paused = false;
	}

	/**
	 * Returns true if target time has been reached, otherwise false.
	 * @return
	 */
	public boolean checkTimer(){
		currentTime = System.currentTimeMillis();
		delta = targetTime - currentTime;
		
		if (targetTime != -1){
			if (delta > 0){
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Sets colour theme for the timer
	 */
	public void setTheme(String theme){
		if (theme == "Light"){
			textColor = Color.BLACK;
		}
		else{
			textColor = Color.WHITE;
		}
	}

	/**
	 * Draws timer on Graphics Context
	 */
	public void draw(GraphicsContext gc){
		//if timer is paused, display paused
		if (paused){
			gc.setFont(Font.font ("Arial", 48));
			gc.setFill(textColor);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			gc.fillText("PAUSED", Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2));
			gc.setFont(Font.font ("Arial", 24));
			gc.fillText("Press P to unpause", Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2)+56);
		}
		else {
			
			//3 second countdown timer is in seconds only format
			if (mode == 1) {
				
				//work out units from system millisecond time
				sec = (targetTime - currentTime) / 1000;
				mins = sec / 60;
				sec = sec - mins*60;

				//convert seconds to string
				if (sec > 0){
					timeLeft = Long.toString(sec);
				}
				//if time is at zero, show GO!
				else{
					timeLeft = "GO!";
				}

				//draw timeLeft
				gc.setFont(Font.font ("Arial", 60));
				gc.setFill(textColor);
				gc.setTextAlign(TextAlignment.CENTER);
				gc.setTextBaseline(VPos.CENTER);
				gc.fillText(timeLeft, Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2));
			}
			
			//standard game timer
			else if (mode == 2) {
				
				if (sec == 0) {
					currentTime = System.currentTimeMillis();
				}
				
				//work out units from system millisecond time
				sec = (targetTime - currentTime) / 1000;
				mins = sec / 60;
				sec = sec - mins*60;
				
				//if time is up, draw TIME'S UP!
				if (currentTime > targetTime) {
					gc.setFont(Font.font ("Arial", 48));
					gc.setFill(textColor);
					gc.setTextAlign(TextAlignment.CENTER);
					gc.setTextBaseline(VPos.CENTER);
					gc.fillText("TIME'S UP!", Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2));

					gc.setFont(Font.font ("Arial", 24));
					gc.fillText("Press ENTER To Restart", Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2+128));
				}
				
				//if a player has one, draw "Player X wins!"
				else if (!GameController.getGame().isGameRunning()) {
					gc.setFont(Font.font ("Arial", 48));
					gc.setFill(textColor);
					gc.setTextAlign(TextAlignment.CENTER);
					gc.setTextBaseline(VPos.CENTER);
					gc.fillText("Player " + GameController.getGame().getWinner() + " wins!", GameWindow.WIDTH/2, GameWindow.HEIGHT/2);
					gc.setFont(Font.font ("Arial", 24));
					gc.fillText("Press ENTER To Restart", Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2+128));
				}
				
				//otherwise, draw the time only in the format M:SS
				else {
					if (sec < 10) {
						timeLeft = mins + ":" + "0" + sec;
					} else {
						timeLeft = mins + ":" + sec;
					}
					gc.setFont(Font.font ("Arial", 60));
					gc.setFill(textColor);
					gc.setTextAlign(TextAlignment.CENTER);
					gc.setTextBaseline(VPos.CENTER);
					gc.fillText(timeLeft, Math.round(GameWindow.WIDTH/2), Math.round(GameWindow.HEIGHT/2));
				}
			}
		}
	}			
}
