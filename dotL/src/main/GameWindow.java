package main;

import java.util.HashSet;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * GameWindow is the entry point to the application and acts as a boundary between the actor(user)
 * and the core program (Controller). It scans the keyboard for inputs and places the commands
 * in HashSet for the controller to use. The GameWindow also runs on an animation loop, calling
 * positional and game logic updates in addition to drawing the appropriate graphics within the application
 * window.
 * @author William Chao and Eugene Fong
 */


public class GameWindow extends Application {

	/**
	 * The width of the game window
	 */
	public static final int WIDTH = 1024;
	
	/**
	 * The height of the game window
	 */
	public static final int HEIGHT = 768;
	
	/**
	 * The size of single 'block' of the game
	 */
	public static final int SIZE = 32;
	
	/**
	 * An instance of the game is created
	 */
	private GameController game;
	
	/**
	 * A scene is created for the canvas to be drawn upon
	 */
	private Scene scene;
	
	/**
	 * Creates a group of objects (children) to be rendered.
	 */
	private Group root;
	
	/**
	 * A HashSet of Strings is used temporarily store the keys which are currently pressed down.
	 * This will be used by the GameController to bind to all in game movement (i.e. paddles) and
	 * game control (i.e. start, stop, pause)
	 */
	private static HashSet<String> currentlyActiveKeys;
	
	/**
	 * Entry point to the JavaFX Application
	 * @param stage - primary stage used by the JavaFX application
	 */
    @Override
    public void start(Stage primaryStage) throws Exception {
    	
    	root = new Group(); 								//Initialise new group
    	
    	//Initialise new game controller, primaryStage and root are used for drawing
    	game = new GameController(root);		
    	scene = new Scene(root, Color.BLACK);				//new scene is created as a base for all drawings
    	primaryStage.setTitle("dotL");						//application title
    	primaryStage.setScene(scene);						//set the new scene as the game's primary scene
    	playGame(primaryStage);								//run application game loop
    }
    
	/**
	 * playGame runs the application game loop. It draws directly on the stage provided.
	 * 	 @param stage - primary stage used by the JavaFX application
	 */
    public void playGame(Stage stage) {
		stage.show();										//visibility of stage is shown

		prepareActionHandlers(scene);						//detects and store keyboard events (presses)
		
		//animation timer runs the primary loop
		new AnimationTimer() {
			public void handle(long currentNanoTime) {			
				game.tick();								//game logic and objects move by one step
				game.draw();							//game's new object positions and properties are rendered
			}
		}.start();
		
    }
    
    /**
	 * getActiveKeys is to be called by a GameController to return a HashSet of key that have been pressed since the last loop
	 */
    public static HashSet<String> getActiveKeys(){
    	return currentlyActiveKeys;
    }
    
    /**
 	 * prepareActionHandlers listens for keyboard inputs for the scene and updates the HashSet for the controller use.
 	 * @param scene - the scene in which keyboard inputs are to be scanned and used by the game controller.
 	 */
    private static void prepareActionHandlers(Scene scene) {
    	//use a set so duplicates are not possible
    	currentlyActiveKeys = new HashSet<String>();
    	
    	//put keys into HashSet when key on keyboard is pressed
    	scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				currentlyActiveKeys.add(event.getCode().toString());
			}
    	});
    	
    	//remove keys into HashSet when key on keyboard is released
    	scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				currentlyActiveKeys.remove(event.getCode().toString());
			}  		
    	});
    }
    


    //main function to launch
    public static void main(String[] args) {
        launch(args);
    }
    
    
   
}
