package main;

import java.util.*;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

/**
 * MainMenu controls all the menu related controls and features such as the player select screen,
 * options, help, about and exit.
 * 
 * @author William Chao and Eugene Fong
 */
public class MainMenu {

	/**
	 * List of selectable options in the root of the menu
	 */
	private List<String> root = new ArrayList<String>();

	/**
	 * When true, the root menu is visible
	 */
	private boolean rootVisible;

	/**
	 * Current selection in the root menu
	 */
	private int rootSelection;

	/**
	 * List of selectable options in the options menu
	 */
	private List<String> options = new ArrayList<String>();

	/**
	 * When true, the options menu is visible
	 */
	private boolean optionsVisible;

	/**
	 * Current selection in the options menu
	 */
	private int optionsSelection;

	/**
	 * When true, the player select screen is visible
	 */
	private boolean playerSelectVisible;

	/**
	 * When true, the help screen is visible
	 */
	private boolean helpVisible;

	/**
	 * When true, the about screen is visible
	 */
	private boolean aboutVisible;

	/**
	 * Set default game theme on menu launch
	 */
	private String theme = "Light";

	/**
	 * Set default game volume on menu launch
	 */
	private double volume = 0.5; 

	/**
	 * running is true is main menu is running
	 */
	private boolean running;

	/**
	 * gameMode that will be launched when main menu exits, currently only for default
	 * mode, but can be expanded later on for other game modes.
	 */
	private int gameMode;

	/**
	 * Time since last keyboard input. Acts primarily as a debr.
	 * If idle for 20 secs, demo mode will run
	 */
	private long timeSinceLastInput = 0;

	/**
	 * Default Foreground Colour
	 */
	private Color defaultPrimary = Color.BLACK;

	/**
	 * Default Background Colour
	 */
	private Color defaultBackground = Color.web("f9f9f9");

	/**
	 * Circle selection notifier
	 */
	private Circle c = new Circle(GameWindow.WIDTH/2 - 96, 3*GameWindow.HEIGHT/5, 12);

	/**
	 * Humans array show which players are human and which are AI. If int = 0, player is human,
	 * if int > 0, the skill level of the AI is the integer value
	 */
	private int[] humans = new int[4];

	/**
	 * Last selected skill for a player. For drop in/drop out on player select screen
	 */
	private int[] lastSkill = new int[4];

	/**
	 * Timer to create blinking text on game screens
	 */
	private Timer timer = new Timer();

	/**
	 * Default MainMenu Constructor
	 */
	public MainMenu(){

		//all screen are not visible until ready
		running = false;
		rootVisible = false;
		optionsVisible = false;
		playerSelectVisible = false;
		rootSelection = 0;

		//root menu options
		root.add("Play");
		root.add("Options");
		root.add("Help");
		root.add("About");
		root.add("Exit");

		//options menu options
		options.add("Change Keybinds");
		options.add("Theme: " + theme);
		options.add("Volume : " + String.format("%.0f", volume*10));
		options.add("Back");

		//mid point level for AI
		Arrays.fill(lastSkill, 5);
		Arrays.fill(humans, 5);
	}

	/**
	 * Start initialises the main menu and load root menu
	 */
	public void start(){
		if (!rootVisible && !running){
			rootVisible = true;
			timeSinceLastInput = System.currentTimeMillis();
		}
		running = true;
	}

	/**
	 * isMainMenuRunning returns true if main menu is active
	 * @return running
	 */
	public boolean isMainMenuRunning(){
		return running;
	}

	/**
	 * getGameMode returns the game mode selected by the players
	 */
	public int getGameMode(){
		return gameMode;
	}

	/**
	 * getTheme gets the colour theme (Light or Dark)
	 */
	public String getTheme(){
		return theme;
	}

	/**
	 * Tick runs at each game loop and detects key presses, if no key presses are detected for 20 seconds,
	 * it will run demo mode.
	 */
	public void tick(){

		//only tick if main menu is active
		if (running){		

			//if root visible, check keys that are applicable for root
			if (rootVisible){
				checkKeysRoot();
			}
			//if options visible, check keys that are applicable for options
			else if (optionsVisible){
				checkKeysOptions();

				//if theme changes, change instantly
				if (theme == "Light"){
					defaultPrimary = Color.BLACK;
					defaultBackground = Color.web("f9f9f9");
					options.set(1, "Theme: " + theme);
				}
				else if (theme == "Dark"){
					defaultPrimary = Color.WHITE;
					defaultBackground = Color.web("404040");
					options.set(1, "Theme: " + theme);
				}

				options.set(2,  "Volume: " + String.format("%.0f",  volume*10));
			}
			//if player select screen visible, check keys that are applicable for screen
			else if (playerSelectVisible) {
				checkKeysSelect();
				timer.blinkGen(800);
			}
			//if help visible, check keys that are applicable for help
			else if (helpVisible){
				checkKeys();
			}
			//if about visible, check keys that are applicable for about
			else if (aboutVisible){
				checkKeys();
			}

			//if set volume
			options.set(2,  "Volume: " + String.format("%.0f",  volume*10));

			//run demo mode if no activity for 20 seconds
			if (timeSinceLastInput+20000 < System.currentTimeMillis() && !GameController.getGameRunning()){

				gameMode = 1;
				running = false;
				rootVisible = false;
				optionsVisible = false;
				playerSelectVisible = false;
				helpVisible = false;
				aboutVisible = false;
			}
		}
	}

	/**
	 * Draw, draws all the menu related items
	 */
	public void draw(GraphicsContext gc){

		//make sure menu is running
		if (running){

			//draw dotL logo on a majority of screens
			if ((!playerSelectVisible) || (!helpVisible) || (!aboutVisible)) {
				gc.setFill(defaultBackground);
				gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);
				gc.setFill(defaultPrimary);
				gc.fillOval(GameWindow.WIDTH/2-64-16, 2*GameWindow.HEIGHT/5-64, 64, 64);
				gc.fillRect(GameWindow.WIDTH/2+16, 2*GameWindow.HEIGHT/5-208, 48, 208);
				gc.setTextAlign(TextAlignment.CENTER);
				gc.setTextBaseline(VPos.CENTER);
				gc.setFont(Font.font ("Arial", FontWeight.BOLD, 48));
				gc.fillText("(dot)L", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/5+56);
			}

			//if root is active, draw root
			if (rootVisible){
				drawRoot(gc);
			}
			//if options is active, draw options
			if (optionsVisible){
				drawOptions(gc);
			}
			//if player select is active, draw player select
			if (playerSelectVisible){
				drawSelect(gc);
			}
			//if help is active, draw help
			if (helpVisible){
				drawHelp(gc);
			}
			//if about is active, draw about
			if (aboutVisible){
				drawAbout(gc);
			}
		}
	}

	/**
	 * checkKeysRoot detects which selection is currently active and performs the appropriate
	 * action (i.e. disabling root screen and showing new screen).
	 */
	private void checkKeysRoot() {
		if (System.currentTimeMillis()-timeSinceLastInput > 150){
			//if enter is pressed
			if (GameWindow.getActiveKeys().contains("ENTER")) {
				timeSinceLastInput = System.currentTimeMillis(); 		//update deb

				//check which option is currently selected
				switch (rootSelection) {

					//play game
				case 0: 
					running = true;
					rootVisible = false;
					optionsVisible = false;
					playerSelectVisible = true;
					helpVisible = false;
					aboutVisible = false;
					break;

					//options menu
				case 1: 
					running = true;
					rootVisible = false;
					optionsVisible = true;
					playerSelectVisible = false;
					helpVisible = false;
					aboutVisible = false;
					break;
					
					//help game
				case 2: 
					running = true;
					rootVisible = false;
					optionsVisible = false;
					playerSelectVisible = false;
					helpVisible = true;
					aboutVisible = false;
					break;
					
					//about game
				case 3: 
					running = true;
					rootVisible = false;
					optionsVisible = false;
					playerSelectVisible = false;
					helpVisible = false;
					aboutVisible = true;
					break;
					
					//exit game
				case 4: 
					running = false;
					rootVisible = false;
					optionsVisible = false;
					playerSelectVisible = false;
					helpVisible = false;
					aboutVisible = false;
					System.exit(0);
					break;
				}
			}

			//up and down change the current selection
			if (GameWindow.getActiveKeys().contains("UP")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (rootSelection > 0){
					rootSelection -= 1;
					GameController.playSound("/a.mp3");
				}
			}
			if (GameWindow.getActiveKeys().contains("DOWN")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (rootSelection < root.size()-1){
					rootSelection += 1;
					GameController.playSound("/a.mp3");
				}
			}
		}
	}

	/**
	 * checkKeysOptions detects which selection is currently active and performs the appropriate
	 * action (i.e. volume or theme).
	 */
	private void checkKeysOptions() {
		if (System.currentTimeMillis()-timeSinceLastInput > 150){
			
			//up and down change the current selection
			if (GameWindow.getActiveKeys().contains("UP")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (optionsSelection > 0){
					optionsSelection -= 1;
					GameController.playSound("/a.mp3");
				}
			}
			if (GameWindow.getActiveKeys().contains("DOWN")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (optionsSelection < options.size()-1){
					optionsSelection += 1;
					GameController.playSound("/a.mp3");
				}
			}
			
			//left and right change theme and volume if on correct selection
			if (GameWindow.getActiveKeys().contains("LEFT")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (optionsSelection == 1){

					if (theme == "Dark"){
						theme = "Light";
					} else{
						theme = "Dark";
					}	
				}

				if (optionsSelection == 2) {

					if (volume - 0.1 > 0.0) {
						volume -= 0.1;
						GameController.playSound("/b.wav");
					} else {
						volume = 0.0;
						GameController.playSound("/b.wav");
					}
				}
			}
			if (GameWindow.getActiveKeys().contains("RIGHT")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (optionsSelection == 1) {

					if (theme == "Dark") {
						theme = "Light";
					} else {
						theme = "Dark";
					}
				}

				if (optionsSelection == 2) {
					if (volume + 0.1 < 1.0) {
						volume += 0.1;
						GameController.playSound("/b.wav");
					} else {
						volume = 1.0;
						GameController.playSound("/b.wav");
					}
				}
			}

			//enter changes theme when pressed or exits back to root menu
			if (GameWindow.getActiveKeys().contains("ENTER")) {
				timeSinceLastInput = System.currentTimeMillis();

				switch(optionsSelection){
				case 0:
					break;
				case 1:
					if (theme == "Dark"){
						theme = "Light";
					}
					else{
						theme = "Dark";
					}
					break;
				case 2:
					break;
				case 3:
					optionsVisible = false;
					rootVisible = true;
					optionsSelection = 0;
					break;
				}
			}

			//exit to root menu
			if (GameWindow.getActiveKeys().contains("ESCAPE")) {
				timeSinceLastInput = System.currentTimeMillis();

				optionsVisible = false;
				rootVisible = true;
				optionsSelection = 0;
			}
		}	
	}

	/**
	 * checkKeysOptions detects which players are to be human or AI. If AI,
	 * it remembers the skill level for the initialisation of the game. left and
	 * right keys change AI difficulty and middle key toggles from AI to human.
	 */
	private void checkKeysSelect() {
		if (System.currentTimeMillis()-timeSinceLastInput > 150){

			//Player 1
			if (GameWindow.getActiveKeys().contains("DIGIT1")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[0] > 1){
					lastSkill[0] -= 1;
				}
			}
			if (GameWindow.getActiveKeys().contains("DIGIT2")) {
				timeSinceLastInput = System.currentTimeMillis();
				GameController.playSound("/b.wav");
				if (humans[0] > 0){
					humans[0] = 0;
				}
				else {
					humans[0] = lastSkill[0];
				}
			}
			if (GameWindow.getActiveKeys().contains("DIGIT3")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[0] < 10){
					lastSkill[0] += 1;
				}
			}

			//Player 2
			if (GameWindow.getActiveKeys().contains("C")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[1] > 1){
					lastSkill[1] -= 1;
				}
			}
			if (GameWindow.getActiveKeys().contains("V")) {
				timeSinceLastInput = System.currentTimeMillis();
				GameController.playSound("/b.wav");
				if (humans[1] > 0){
					humans[1] = 0;
				}
				else {
					humans[1] = lastSkill[1];
				}
			}
			if (GameWindow.getActiveKeys().contains("B")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[1] < 10){
					lastSkill[1] += 1;
				}
			}

			//Player 3
			if (GameWindow.getActiveKeys().contains("DIGIT7")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[2] > 1){
					lastSkill[2] -= 1;
				}
			}
			if (GameWindow.getActiveKeys().contains("DIGIT8")) {
				timeSinceLastInput = System.currentTimeMillis();
				GameController.playSound("/b.wav");
				if (humans[2] > 0){
					humans[2] = 0;
				}
				else {
					humans[2] = lastSkill[2];
				}
			}
			if (GameWindow.getActiveKeys().contains("DIGIT9")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[2] < 10){
					lastSkill[2] += 1;
				}
			}

			//Player 1
			if (GameWindow.getActiveKeys().contains("LEFT")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[3] > 1){
					lastSkill[3] -= 1;
				}
			}
			if (GameWindow.getActiveKeys().contains("DOWN")) {
				timeSinceLastInput = System.currentTimeMillis();
				GameController.playSound("/b.wav");
				if (humans[3] > 0){
					humans[3] = 0;
				}
				else {
					humans[3] = lastSkill[3];
				}
			}
			if (GameWindow.getActiveKeys().contains("RIGHT")) {
				timeSinceLastInput = System.currentTimeMillis();

				if (lastSkill[3] < 10){
					lastSkill[3] += 1;
				}
			}
			
			//Enter starts the game, closes main menu and sets game mode
			if (GameWindow.getActiveKeys().contains("ENTER")) {
				timeSinceLastInput = System.currentTimeMillis();

				for (int i = 0; i < humans.length; i++) {
					if(humans[i] > 0) {
						humans[i] = lastSkill[i];
					}
				}
				gameMode = 1;
				playerSelectVisible = false;
				running = false;

			}

			//Escape returns to the main menu
			if (GameWindow.getActiveKeys().contains("ESCAPE")){
				timeSinceLastInput = System.currentTimeMillis();

				for(int i = 0; i < 4; i++) {
					humans[i] = 0;
				}

				playerSelectVisible = false;
				optionsVisible = false;
				helpVisible = false;
				aboutVisible = false;
				rootVisible = true;
				running = true;
			}
		}
	}

	/**
	 * check keys checks keys for information screens (i.e. help and about screens)
	 * primarily for escape functionality.
	 * */
	private void checkKeys(){
		if (helpVisible){
			if (GameWindow.getActiveKeys().contains("ESCAPE")){
				timeSinceLastInput = System.currentTimeMillis();

				playerSelectVisible = false;
				optionsVisible = false;
				rootVisible = true;
				helpVisible = false;
				aboutVisible = false;
				running = true;
			}
		}
		else if (aboutVisible){
			if (GameWindow.getActiveKeys().contains("ESCAPE")){
				timeSinceLastInput = System.currentTimeMillis();

				playerSelectVisible = false;
				optionsVisible = false;
				rootVisible = true;
				helpVisible = false;
				aboutVisible = false;
				running = true;
			}
		}
	}

	/**
	 * Draws screen of root to Graphics Context
	 * */
	private void drawRoot(GraphicsContext gc){
		
		//draw list of options
		for (int i = 0; i < root.size(); i++){
			gc.setTextAlign(TextAlignment.LEFT);
			gc.setTextBaseline(VPos.CENTER);
			if (rootSelection == i){
				gc.setFont(Font.font ("Arial", FontWeight.BOLD, 32));
			}
			else{
				gc.setFont(Font.font ("Arial", 32));
			}
			gc.fillText(root.get(i), GameWindow.WIDTH/2-64-4, 3*GameWindow.HEIGHT/5+46*i);
		}

		//change dot colour
		switch (rootSelection){
		case 0:
			gc.setFill(Color.web("c83737"));
			break;
		case 1:
			gc.setFill(Color.web("00aad4"));
			break;
		case 2:
			gc.setFill(Color.web("ff6600"));
			break;
		case 3:
			gc.setFill(Color.web("008000"));
			break;
		case 4:
			gc.setFill(defaultPrimary);
			break;
		}
		//draw dot to correspond to selection
		gc.fillOval(c.getCenterX()-c.getRadius(), c.getCenterY()-c.getRadius()+rootSelection*46, c.getRadius()*2, c.getRadius()*2);
	}

	/**
	 * Draws screen of options to Graphics Context
	 * */
	private void drawOptions(GraphicsContext gc){
		
		//draw list of options
		for (int i = 0; i < options.size(); i++){
			gc.setTextAlign(TextAlignment.LEFT);
			gc.setTextBaseline(VPos.CENTER);
			if (optionsSelection == i){
				gc.setFont(Font.font ("Arial", FontWeight.BOLD, 32));
			} else {
				gc.setFont(Font.font ("Arial", 32));
			}
			gc.fillText(options.get(i), GameWindow.WIDTH/2-64-4, 3*GameWindow.HEIGHT/5+46*i);
		}

		//change dot colour
		switch (optionsSelection){
		case 0:
			gc.setFill(Color.web("c83737"));
			break;
		case 1:
			gc.setFill(Color.web("00aad4"));
			break;
		case 2:
			gc.setFill(Color.web("ff6600"));
			break;
		case 3:
			gc.setFill(Color.web("008000"));
			break;
		case 4:
			gc.setFill(defaultPrimary);
			break;
		}
		
		//draw dot to correspond to selection
		gc.fillOval(c.getCenterX()-c.getRadius(), c.getCenterY()-c.getRadius()+optionsSelection*46, c.getRadius()*2, c.getRadius()*2);
	}

	/**
	 * Draws the player select screen to Graphics Context
	 * */
	private void drawSelect(GraphicsContext gc){

		//background fill to remove dotL logo
		gc.setFill(defaultBackground);
		gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);

		//Player 1 Warlord
		gc.setFill(Color.web("c83737"));
		gc.fillOval(75, 40, 100, 100);

		//Player 2 Warlord
		gc.setFill(Color.web("00aad4"));
		gc.fillOval(75, 627, 100, 100);

		//Player 3 Warlord
		gc.setFill(Color.web("ff6600"));
		gc.fillOval(850, 40, 100, 100);

		//Player 4 Warlord
		gc.setFill(Color.web("008000"));
		gc.fillOval(850, 627, 100, 100);

		//Text for players 1-4. If humans[x] = true then don't fill that text.
		gc.setFill(defaultPrimary);
		gc.setTextAlign(TextAlignment.CENTER);
		gc.setTextBaseline(VPos.CENTER);
		gc.setFont(Font.font ("Arial", FontWeight.BOLD, 48));

		//blink information on screen
		if (timer.checkBlink()){
			for(int i = 0; i < humans.length; i++) {
				//if humans are present in the game
				if (humans[i] == 0) {
					gc.fillText("Press ENTER to begin", GameWindow.WIDTH/2, GameWindow.HEIGHT/2);
					break;
				}
				//if no human players present show demo mode
				if(i == humans.length-1) {
					gc.fillText("PLAYER SELECT", GameWindow.WIDTH/2, GameWindow.HEIGHT/2);
					gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
					gc.fillText("Press ENTER to start Demo", GameWindow.WIDTH/2, GameWindow.HEIGHT/2+48);
				}
			}
		}

		//draw player on the left side
		gc.setTextAlign(TextAlignment.LEFT);

		//player 1
		if(humans[0] > 0) {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("Press '2' to join", 75, 174);
			gc.fillText("AI Level " + Integer.toString(lastSkill[0]), 75, 174+64);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press '1' or '3' to change AI", 75, 174+28);
		} else {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press '2' to leave", 75, 174);
		}

		//player 2
		if(humans[1] > 0) {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("Press 'V' to join", 75, 593);
			gc.fillText("AI Level " + Integer.toString(lastSkill[1]), 75, 593-64);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press 'C' or 'B' to change AI", 75, 593-28);
		} else {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press 'V' to leave", 75, 593);
		}

		//draw player on the right side
		gc.setTextAlign(TextAlignment.RIGHT);
		//player 3
		if(humans[2] > 0) {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("Press '8' to join", 950, 174);
			gc.fillText("AI Level " + Integer.toString(lastSkill[2]), 950, 174+64);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press '7' or '9' to change AI", 950, 174+28);
		} else {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press '8' to leave", 950, 174);
		}

		//player 4
		if(humans[3] > 0) {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
			gc.fillText("Press 'DOWN' to join", 950, 593);
			gc.fillText("AI Level " + Integer.toString(lastSkill[3]), 950, 594-64);
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press 'LEFT' or 'RIGHT' to change AI", 950, 593-28);
		} else {
			gc.setFont(Font.font("Arial", FontWeight.BOLD, 16));
			gc.fillText("Press 'DOWN' to leave", 950, 593);
		}
	}

	/**
	 * Draws the help screen to Graphics Context
	 * */
	public void drawHelp(GraphicsContext gc){
		gc.setFill(defaultBackground);
		gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);

		gc.setFill(defaultPrimary);
		gc.setTextAlign(TextAlignment.LEFT);
		gc.setTextBaseline(VPos.CENTER);
		gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		gc.fillText("The game Warlords is most compared to the classic game of pong,\n"
				+ "but with four-players.\n\n"
				+ "Each player is allocated their own corner and is surrounded in \n"
				+ "their own base and wall structure. A ball is spawned at the beginning\n"
				+ "of the game and will destroy all objects (i.e. base, walls) except for\n"
				+ "the paddle. Each player�s paddle can be controlled if the player�s\n"
				+ "base is still active and is able to deflect the ball away from the \n"
				+ "player�s corner.\n\n"
				+ "The primary objective of the game is to be the last remaining player\n"
				+ "alive with the most amount of walls remaining.", GameWindow.WIDTH/8, GameWindow.HEIGHT/2);
		gc.setTextAlign(TextAlignment.CENTER);
		gc.fillText("Press 'ESC' to exit", GameWindow.WIDTH/2, 5*GameWindow.HEIGHT/6);
	}

	/**
	 * Draws the about screen to Graphics Context
	 * */
	public void drawAbout(GraphicsContext gc){
		gc.setFill(defaultBackground);
		gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);

		gc.setFill(defaultPrimary);
		gc.setTextAlign(TextAlignment.CENTER);
		gc.setTextBaseline(VPos.CENTER);
		gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		gc.fillText("dotL is designed by", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/8-64);
		gc.setFont(Font.font("Arial", FontWeight.BOLD, 48));
		gc.fillText("THE 24th GAME STUDIO", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/8);
		gc.setFont(Font.font("Arial", FontWeight.BOLD, 24));
		gc.fillText("Eugene and William", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/8+64);
		gc.fillText("Sound sourced from", GameWindow.WIDTH/2, 4*GameWindow.HEIGHT/8-36);
		gc.fillText("soundbible.com (Creative Attribution 3.0)", GameWindow.WIDTH/2, 4*GameWindow.HEIGHT/8);
		gc.fillText("Thanks Andrew.", GameWindow.WIDTH/2, 5*GameWindow.HEIGHT/8);
		gc.fillText("Thanks Hammond and Michael.", GameWindow.WIDTH/2, 5*GameWindow.HEIGHT/8+36);
		gc.fillText("Thanks COMPSYS302.", GameWindow.WIDTH/2, 5*GameWindow.HEIGHT/8+72);
		gc.fillText("Most of all, thanks Obama.", GameWindow.WIDTH/2, 5*GameWindow.HEIGHT/8+108);
		gc.fillText("Press 'ESC' to exit", GameWindow.WIDTH/2, 7*GameWindow.HEIGHT/8);
	}

	/**
	 * getHumans gets the humans array to indicate which players are AI and which players are humans
	 * @return humans
	 * */
	public int[] getHumans() {
		return humans;
	}

	/**
	 * getVolume returns the set volume to the game
	 * @return volume
	 * */
	public double getVolume() {
		return volume;
	}

}
