package main;

import java.util.ArrayList;
import java.util.List;

import entity.Ball;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

/**
 * The SplashScreen class is the first screen shown when the game starts up.
 * It contains the logo with four balls bouncing around with a blinking message
 * waiting for the user to press enter to get to the main menu.
 * @author William Chao and Eugene Fong
 *
 */
public class SplashScreen {
	
	/**
	 * Returns true if splash screen is running else false
	 */
	private boolean running;
	
	/**
	 * The size of the balls
	 */
	private int size;
	
	/**
	 * Contains a list of balls
	 */
	private List<Ball> balls = new ArrayList<Ball>();
	
	/**
	 * The last time input was pressed used for debouncing
	 */
	private long timeSinceLastInput;
	
	/**
	 * Used to make text message blink every few seconds
	 */
	private Timer blinker = new Timer();
	
	/**
	 * Default constructor sets timeSinceLastInput to 0, size of balls to 48,
	 * sets running to true and creates four balls 
	 */
	public SplashScreen(){
		timeSinceLastInput = 0;
		size = 48;
		running = true;
		
		balls.add(new Ball(0, 0, 10, 10, Color.web("c83737")));
		balls.add(new Ball(GameWindow.WIDTH-size, 96, -10, 6, Color.web("00aad4")));
		balls.add(new Ball(256, GameWindow.HEIGHT-size, 8, -10, Color.web("ff6600")));
		balls.add(new Ball(GameWindow.WIDTH-size, GameWindow.HEIGHT-size-48, -10, -9, Color.web("008000")));
		
		for (int i = 0; i < balls.size(); i++) {
			balls.get(i).setSize(size);
		}
	}

	/**
	 * 
	 * @return true if the splash screen is running else false
	 */
	public boolean isSplashScreenRunning(){
		return running;
	}

	/**
	 * Checks for input then updates balls positions and sets blinker to true/false
	 * after 800 ms
	 */
	public void tick(){
		if (running){
			checkKeys();
			
			for(Ball i : balls) {
				i.update();
			}

			blinker.blinkGen(800);
		}
	}
	
	/**
	 * Draws the background, logo, text and balls on the screen.
	 * @param gc - the graphics context of the canvas
	 */
	public void draw(GraphicsContext gc){
		if (running){
			//Background
			gc.setFill(Color.web("f9f9f9"));
			gc.fillRect(0, 0, GameWindow.WIDTH, GameWindow.HEIGHT);
			
			//Balls
			for(Ball i : balls) {
				i.draw(gc);
			}

			//Title & Logo
			gc.setFill(Color.BLACK);
			gc.fillOval(GameWindow.WIDTH/2-64-16, 2*GameWindow.HEIGHT/5-64, 64, 64);
			gc.fillRect(GameWindow.WIDTH/2+16, 2*GameWindow.HEIGHT/5-208, 48, 208);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.setTextBaseline(VPos.CENTER);
			gc.setFont(Font.font ("Arial", FontWeight.BOLD, 48));
			gc.fillText("(dot)L", GameWindow.WIDTH/2, 2*GameWindow.HEIGHT/5+56);
			
			//Blinking press enter to start message
			if (blinker.checkBlink()){
				gc.setFont(Font.font ("Arial", FontWeight.BOLD, 32));
				gc.fillText("Press ENTER To Start", GameWindow.WIDTH/2, 3*GameWindow.HEIGHT/4);
			}
		}
	}
	
	/**
	 * Checks if the user has pressed enter to stop running the spalsh screen
	 */
	private void checkKeys() {

		if (GameWindow.getActiveKeys().contains("ENTER")) {
			if (System.currentTimeMillis()-timeSinceLastInput > 250){
				timeSinceLastInput = System.currentTimeMillis();
				running = false;
			}
		}
	}
}
