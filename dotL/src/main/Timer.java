package main;

/**
 * The timer does generates a random delay for AI and is used to
 * create the blinking effects on the splash screen and main menu.
 * 
 * @author William Chao and Eugene Fong
 */
public class Timer {

	/**
	 * The set time that the timer is to count up to
	 */
	private long timeToCount;
	
	/**
	 * The system time that the timer is to count up to
	 */
	private long targetTime = 0;
	
	/**
	 * The current system time
	 */
	private long currentTime = 0;
	
	/**
	 * The difference between the target time and system time
	 */
	private long delta = 0;
	
	/**
	 * The last time blink has occurred
	 */
	private long lastBlink;
	
	/**
	 * if blink should occur, blink should be true
	 */
	private boolean blink;

	public Timer(){
	}

	/**
	 * setRandom creates a random delay in a certain range to manipulate the effectiveness of AI.
	 * If AI level is set to maximum, time delay is zero. If AI level is at the minimum, AI has
	 * a higher chance of having a slower reaction speed when compared to a higher AI level.
	 * @param range
	 */
	public void setRandom(int range){
		timeToCount = (long) (Math.random()*(double) range)*500;
		targetTime = System.currentTimeMillis()+timeToCount;
		currentTime = System.currentTimeMillis();
	}

	/**
	 * Resets the timer
	 */
	public void reset(){
		targetTime = System.currentTimeMillis()+timeToCount;
		currentTime = System.currentTimeMillis();
	}

	/**
	 * @return Checks if timer has reached the target time (time to count) and becomes true if time is met.
	 */
	public boolean checkTimer(){
		currentTime = System.currentTimeMillis();
		delta = targetTime - currentTime;

		if (delta > 0){
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * Blink Generator, toggles blink on and off at the desired rate
	 * @param millis
	 */
	public void blinkGen(int millis){
		if ((System.currentTimeMillis()-lastBlink > millis) && (blink == false)){
			lastBlink = System.currentTimeMillis();
			blink = true;
		}
		else if ((System.currentTimeMillis()-lastBlink > millis) && (blink == true)){
			lastBlink = System.currentTimeMillis();
			blink = false;
		}
	}

	/**
	 * Check to see if blink is toggled on or off.
	 */
	public boolean checkBlink(){
		return blink;
	}
}
